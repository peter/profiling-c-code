SUMMARY = "GE Matrix Demo for Perf"
LICENSE = "Proprietary"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/Proprietary;md5=0557f9d92cf58f2ccdd50f62f8ac0b28"

SRC_URI = "file://ge-matrix.c"

do_compile () {
        ${CC} -lm -g ${WORKDIR}/ge-matrix.c -o ${WORKDIR}/ge-matrix
        ${CC} -lm -g -O4 ${WORKDIR}/ge-matrix.c -o ${WORKDIR}/ge-matrix.opt
}

do_install () {
        install -d ${D}${bindir}
        install -m 0755 ${WORKDIR}/ge-matrix ${D}${bindir}/
        install -m 0755 ${WORKDIR}/ge-matrix.opt ${D}${bindir}/
}

