CFLAGS	= -lm -g
RM	= rm -f

default:
	$(CC) $(CFLAGS) -o ge-matrix ge-matrix.c
	$(CC) $(CFLAGS) -O4 -o ge-matrix.opt ge-matrix.c

clean:
	$(RM) ge-matrix ge-matrix.opt
